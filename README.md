# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## Instructions:
---------------
Author: Newton Blair

Contact Address: nblair@uoregon.edu

Description of Software:
Running this Software should result in Hello world getting printed.
